# Iterate
Generates all possible combinations of a given set of characters, to a given length.

## Requirements

* PHP 7
* Composer

## Usage Instructions

1. Clone repo to your machine.
2. Set options in `.env`, using `.env.example` as a template
3. Run `composer install`
3. Run `php Iterate.php`
4. Wait! Depending on your total number of characters, and the length of your output, this can take a while.
5. Your output will be found in the `output` directory, and will be separated by length.

## PLEASE NOTE!
This program can take a while for large lengths of combinations, this hasn't been optimised for speed, but for length!

Bellow are some sample run times, based on an Amazon AWS EC2 `t2.small` with just lowercase letters `a` thru `z`

Length | Combinations Generated | Total Time
------ | ---------------------- | ----------
1      | 26                     | 0.003s
2      | 702                    | 0.002s
3      | 18,278                 | 0.365s
4      | 475,254                | 8.497s
5      | 12,356,630             | 224.427s

As you can no doubt see the longer the string, the execution time will increase dramatically!
